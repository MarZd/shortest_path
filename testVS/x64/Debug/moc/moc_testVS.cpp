/****************************************************************************
** Meta object code from reading C++ file 'testVS.h'
**
** Created by: The Qt Meta Object Compiler version 68 (Qt 6.2.2)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include <memory>
#include "../../../testVS.h"
#include <QtGui/qtextcursor.h>
#include <QScreen>
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'testVS.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 68
#error "This file was generated using the moc from 6.2.2. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_testVS_t {
    const uint offsetsAndSize[38];
    char stringdata0[243];
};
#define QT_MOC_LITERAL(ofs, len) \
    uint(offsetof(qt_meta_stringdata_testVS_t, stringdata0) + ofs), len 
static const qt_meta_stringdata_testVS_t qt_meta_stringdata_testVS = {
    {
QT_MOC_LITERAL(0, 6), // "testVS"
QT_MOC_LITERAL(7, 13), // "wroclaw_label"
QT_MOC_LITERAL(21, 0), // ""
QT_MOC_LITERAL(22, 14), // "warszawa_label"
QT_MOC_LITERAL(37, 14), // "szczecin_label"
QT_MOC_LITERAL(52, 12), // "gdansk_label"
QT_MOC_LITERAL(65, 13), // "olsztyn_label"
QT_MOC_LITERAL(79, 15), // "bydgoszcz_label"
QT_MOC_LITERAL(95, 15), // "bialystok_label"
QT_MOC_LITERAL(111, 12), // "krakow_label"
QT_MOC_LITERAL(124, 12), // "poznan_label"
QT_MOC_LITERAL(137, 14), // "zielonag_label"
QT_MOC_LITERAL(152, 12), // "lublin_label"
QT_MOC_LITERAL(165, 14), // "katowice_label"
QT_MOC_LITERAL(180, 13), // "rzeszow_label"
QT_MOC_LITERAL(194, 12), // "kielce_label"
QT_MOC_LITERAL(207, 10), // "lodz_label"
QT_MOC_LITERAL(218, 11), // "opole_label"
QT_MOC_LITERAL(230, 12) // "oblicz_droge"

    },
    "testVS\0wroclaw_label\0\0warszawa_label\0"
    "szczecin_label\0gdansk_label\0olsztyn_label\0"
    "bydgoszcz_label\0bialystok_label\0"
    "krakow_label\0poznan_label\0zielonag_label\0"
    "lublin_label\0katowice_label\0rzeszow_label\0"
    "kielce_label\0lodz_label\0opole_label\0"
    "oblicz_droge"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_testVS[] = {

 // content:
      10,       // revision
       0,       // classname
       0,    0, // classinfo
      17,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags, initial metatype offsets
       1,    0,  116,    2, 0x08,    1 /* Private */,
       3,    0,  117,    2, 0x08,    2 /* Private */,
       4,    0,  118,    2, 0x08,    3 /* Private */,
       5,    0,  119,    2, 0x08,    4 /* Private */,
       6,    0,  120,    2, 0x08,    5 /* Private */,
       7,    0,  121,    2, 0x08,    6 /* Private */,
       8,    0,  122,    2, 0x08,    7 /* Private */,
       9,    0,  123,    2, 0x08,    8 /* Private */,
      10,    0,  124,    2, 0x08,    9 /* Private */,
      11,    0,  125,    2, 0x08,   10 /* Private */,
      12,    0,  126,    2, 0x08,   11 /* Private */,
      13,    0,  127,    2, 0x08,   12 /* Private */,
      14,    0,  128,    2, 0x08,   13 /* Private */,
      15,    0,  129,    2, 0x08,   14 /* Private */,
      16,    0,  130,    2, 0x08,   15 /* Private */,
      17,    0,  131,    2, 0x08,   16 /* Private */,
      18,    0,  132,    2, 0x08,   17 /* Private */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void testVS::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<testVS *>(_o);
        (void)_t;
        switch (_id) {
        case 0: _t->wroclaw_label(); break;
        case 1: _t->warszawa_label(); break;
        case 2: _t->szczecin_label(); break;
        case 3: _t->gdansk_label(); break;
        case 4: _t->olsztyn_label(); break;
        case 5: _t->bydgoszcz_label(); break;
        case 6: _t->bialystok_label(); break;
        case 7: _t->krakow_label(); break;
        case 8: _t->poznan_label(); break;
        case 9: _t->zielonag_label(); break;
        case 10: _t->lublin_label(); break;
        case 11: _t->katowice_label(); break;
        case 12: _t->rzeszow_label(); break;
        case 13: _t->kielce_label(); break;
        case 14: _t->lodz_label(); break;
        case 15: _t->opole_label(); break;
        case 16: _t->oblicz_droge(); break;
        default: ;
        }
    }
    (void)_a;
}

const QMetaObject testVS::staticMetaObject = { {
    QMetaObject::SuperData::link<QWidget::staticMetaObject>(),
    qt_meta_stringdata_testVS.offsetsAndSize,
    qt_meta_data_testVS,
    qt_static_metacall,
    nullptr,
qt_incomplete_metaTypeArray<qt_meta_stringdata_testVS_t
, QtPrivate::TypeAndForceComplete<testVS, std::true_type>
, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>


>,
    nullptr
} };


const QMetaObject *testVS::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *testVS::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_testVS.stringdata0))
        return static_cast<void*>(this);
    return QWidget::qt_metacast(_clname);
}

int testVS::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 17)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 17;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 17)
            *reinterpret_cast<QMetaType *>(_a[0]) = QMetaType();
        _id -= 17;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
